import { Senha } from 'src/app/models/senha.model';
import { SenhaService } from 'src/app/services/class/senha.service';
import { ClienteService } from 'src/app/services/class/cliente.service';
import { RecursoService } from 'src/app/services/class/recurso.service';
import { Cliente } from 'src/app/models/cliente.model';
import { Recurso } from 'src/app/models/recurso.model';
// default
import { Component, OnInit } from '@angular/core';
import { FormControl, NgControlStatus, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { HttpProgressEvent } from '@angular/common/http';
import { Imagem } from 'src/app/models/imagem.model';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import { Servidor } from 'src/app/models/servidor.model';
import { ServidorService } from 'src/app/services/class/servidor.service';

@Component({
  selector: 'app-senhas-editar',
  templateUrl: './senhas-editar.component.html',
  styleUrls: ['./senhas-editar.component.scss']
})
export class SenhasEditarComponent implements OnInit {

  data: Senha = new Senha();

  clientes: Cliente[] = [];
  imagens: any[] = [];
  searchClientes: string = '';

  recursos: Recurso[] = [];
  searchRecursos: string = '';

  hideSenhaInterno: boolean = true;
  hideSenhaCliente: boolean = true;

  files: File[] = [];
  progress: number = 0;

  myControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;

  isOptionsEmpty: boolean = false;
  servidores: Servidor[] = [];

  constructor(
    public senhaService: SenhaService,
    public clienteService: ClienteService,
    public recursoService: RecursoService,
    public servidorService: ServidorService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService,
    public dialog: MatDialog,

  ) { }

  ngOnInit() {
    this.route.params.subscribe(param => this.buscar(param.id));
    this.filterOptions();
  }


  filterOptions() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    this.isOptionsEmpty = false;
    const filterValue = value.toLowerCase();
    const options = this.options.filter(option => option.toLowerCase().includes(filterValue));
    if (!options.length) {
      this.isOptionsEmpty = true;
    }
    return options;
  }

  onOptionSelected(event: MatAutocompleteSelectedEvent) {
    const nome = event.option.value;
    const cliente = this.clientes.find(cliente => cliente.nome === nome);
    this.data.cliente = cliente;
    console.log(cliente);
  }

  submitCliente(ev) {

    const nome = ev.trim();

    if (this.options.some(value => value === nome)) return;

    const cliente = {
      id: null,
      nome: nome,
      situacao: 'A',
    } as Cliente;

    this.clienteService.post(cliente).subscribe(res => {
      cliente.id = res;
      this.data.cliente = cliente;
      this.options.push(cliente.nome);
      this.helper.openSnackBar('Cliente inserido com sucesso!');
      this.buscarClientes();
    })
  }

  buscar(id: number) {
    this.senhaService.getById(id)
      .subscribe((res: Senha) => {
        this.data = res;
        this.buscarImagens(res.id);
        this.buscarClientes();
        this.buscarRecursos();
        this.buscarServidores();
      });
  }

  buscarClientes() {
    this.clienteService.get(-99, -99, '1', 'ASC', 'A')
      .subscribe((res: any) => {
        this.clientes = res.clientes;
        this.options = res.clientes.map(cliente => cliente.nome);
        const cliente = res.clientes.find(cliente => cliente.id === this.data.cliente.id);
        this.myControl.setValue(cliente.nome);
      });
  }

  getFileExtension(fileName: string) {
    return fileName.split('.').pop().toLocaleLowerCase();
  }

  isImage(arquivo: string): boolean {
    let isImage = false;
    const extension = this.getFileExtension(arquivo);
    if (extension === 'png' || extension === 'jpg' || extension === 'jpeg') {
      isImage = true;
    }
    return isImage;
  }

  buscarServidores() {
    this.servidorService.getSelect()
      .subscribe((res: Servidor[]) => this.servidores = res);
  }

  buscarRecursos() {
    this.recursoService.get(-99, -99, '1', 'ASC')
      .subscribe((res: any) => this.recursos = res.recursos);
  }

  buscarImagens(idSenha: number) {
    this.senhaService.getImagens(idSenha)
      .subscribe((res: Senha[]) => this.imagens = res);
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Preencha os campos requiridos, por favor!');
      return;
    }

    this.loadingService.present('Alterando item...');

    this.senhaService.patch(this.data)
      .subscribe((res: any) => {
        if (this.files.length) {
          this.submitGaleria(this.files, `/senhaArquivo/inserirMultiplo?idSenha=${this.data.id}`);
        }
        this.router.navigate(['/senhas']).then(() => {
          this.helper.openSnackBar('Item inserido com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => {
        this.helper.openSnackBar(e.error);
        this.loadingService.dismiss()
      });
  }

  submitGaleria(files: File[], url: string) {
    if (!files.length) {
      return;
    }
    return new Promise((resolve, reject) => {
      this.senhaService.postImages(files, url, {})
        .subscribe((event: HttpProgressEvent | any) => {
          if (event.type === 4) {
            this.progress = 0;
            resolve(event);
          } else {
            this.progress = Math.round((event.loaded / event.total) * 100);
            if (isNaN(this.progress)) {
              this.progress = 100;
            }
          }
        }, err => reject(err));
    });
  }

  // onSelect(event) {
  //   this.loadingService.present('Inserindo imagens...');
  //   this.files.push(...event.addedFiles);
  //   this.submitGaleria(this.files, `/senhaArquivo/inserir?idSenha=${this.data.id}`)
  //     .then(res => {
  //       this.files = [];
  //       this.buscar(this.data.id);
  //       this.loadingService.dismiss();
  //     }, e => this.loadingService.dismiss());
  // }

  onSelect(event) {

    this.files.push(...event.addedFiles);
    let count = event.addedFiles.length;

    this.loadingService.present('Inserindo');

    event.addedFiles.forEach(file => {
      const url = `/senhaArquivo/inserir?idSenha=${this.data.id}`;
      this.senhaService.postFile(file, url, 'arquivo')
        .subscribe((e: HttpProgressEvent | any) => {
          console.log(e);
          if (e.type === 4) {
            count--;
            if (count == 0) {
              this.progress = 0;
              this.loadingService.dismiss();
              this.files = [];
              this.buscar(this.data.id);
            }
          } else {
            this.progress = Math.round((e.loaded / e.total) * 100);
            if (isNaN(this.progress)) {
              this.progress = 100;
            }
            this.loadingService.title = `${this.progress}%`;
          }
        })
    });
  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  deletarImagem(imagem: Imagem, index: number) {
    this.loadingService.present('Deletando imagem...');
    this.senhaService.deleteImage(imagem.id)
      .subscribe(res => {
        this.imagens.splice(index, 1);
        this.loadingService.dismiss();
      }, e => this.loadingService.dismiss())
  }

  deletarData() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {
        title: 'Excluir item',
        description: 'Você realmente quer excluir esse item? Esse processe não pode ser desfeito.'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadingService.present('Excluindo item...');
        this.senhaService.delete(this.data).subscribe((res: any) => {
          this.router.navigate(['/senhas']);
          this.helper.openSnackBar('Item removido com sucesso.');
          this.loadingService.dismiss();
        }, e => this.loadingService.dismiss());
      }
    })
  }

}

