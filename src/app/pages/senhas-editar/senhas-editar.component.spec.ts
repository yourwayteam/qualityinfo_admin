import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SenhasEditarComponent } from './senhas-editar.component';

describe('SenhasEditarComponent', () => {
  let component: SenhasEditarComponent;
  let fixture: ComponentFixture<SenhasEditarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SenhasEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenhasEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
