import { Servidor } from 'src/app/models/servidor.model';
// Default
import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from './../../services/loading.service';
import { HelperService } from './../../services/helper.service';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { ServidorService } from 'src/app/services/class/servidor.service';

@Component({
  selector: 'app-servidores-editar',
  templateUrl: './servidores-editar.component.html',
  styleUrls: ['./servidores-editar.component.scss']
})
export class ServidoresEditarComponent implements OnInit {

  data: Servidor = new Servidor();

  constructor(
    public servidorService: ServidorService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(param => this.buscar(param.id));
  }

  buscar(id: number) {
    this.servidorService.getById(id)
      .subscribe((res: Servidor) => this.data = res);
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Preencha os campos requiridos, por favor!');
      return;
    }

    this.loadingService.present('Alterando item...');

    this.servidorService.patch(this.data)
      .subscribe((res: any) => {
        this.router.navigate(['/servidores']).then(() => {
          this.helper.openSnackBar('Item alterado com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => {
        this.helper.openSnackBar(e.error);
        this.loadingService.dismiss()
      });
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }
}
