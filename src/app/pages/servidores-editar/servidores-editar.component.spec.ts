import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServidoresEditarComponent } from './servidores-editar.component';

describe('ServidoresEditarComponent', () => {
  let component: ServidoresEditarComponent;
  let fixture: ComponentFixture<ServidoresEditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServidoresEditarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServidoresEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
