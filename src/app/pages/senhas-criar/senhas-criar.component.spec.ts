import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SenhasCriarComponent } from './senhas-criar.component';

describe('SenhasCriarComponent', () => {
  let component: SenhasCriarComponent;
  let fixture: ComponentFixture<SenhasCriarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SenhasCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenhasCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
