import { Senha } from 'src/app/models/senha.model';
import { SenhaService } from 'src/app/services/class/senha.service';
import { ClienteService } from 'src/app/services/class/cliente.service';
import { RecursoService } from 'src/app/services/class/recurso.service';
import { Cliente } from 'src/app/models/cliente.model';
import { Recurso } from 'src/app/models/recurso.model';
// default
import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';
import { LoadingService } from 'src/app/services/loading.service';
import { HttpProgressEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { map, startWith } from 'rxjs/operators';
import { ServidorService } from 'src/app/services/class/servidor.service';
import { Servidor } from 'src/app/models/servidor.model';

@Component({
  selector: 'app-senhas-criar',
  templateUrl: './senhas-criar.component.html',
  styleUrls: ['./senhas-criar.component.scss']
})
export class SenhasCriarComponent implements OnInit {

  data: Senha = new Senha();

  clientes: Cliente[] = [];
  searchClientes: string = '';

  recursos: Recurso[] = [];
  searchRecursos: string = '';

  hideSenhaInterno: boolean = true;
  hideSenhaCliente: boolean = true;

  files: File[] = [];
  progress: number = 0;

  myControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;

  servidores: Servidor[] = [];

  isOptionsEmpty: boolean = false;

  constructor(
    public senhaService: SenhaService,
    public servidorService: ServidorService,
    public clienteService: ClienteService,
    public recursoService: RecursoService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public global: GlobalService
  ) { }

  ngOnInit() {
    this.buscarClientes();
    this.buscarRecursos();
    this.buscarServidores();
    this.filterOptions();
  }

  buscarClientes() {
    this.clienteService.get(-99, -99, '1', 'ASC')
      .subscribe((res: any) => {
        this.clientes = res.clientes;
        this.options = res.clientes.map(cliente => cliente.nome);
      });
  }

  buscarServidores() {
    this.servidorService.getSelect()
      .subscribe((res: Servidor[]) => this.servidores = res);
  }

  buscarRecursos() {
    this.recursoService.get(-99, -99, '1', 'ASC')
      .subscribe((res: any) => this.recursos = res.recursos);
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Preencha os campos requiridos, por favor!');
      return;
    }

    this.loadingService.present('Inserindo item...');

    this.senhaService.post(this.data)
      .subscribe(res => {
        let count = this.files.length;
        if (count) {
          this.files.forEach(file => {
            const url = `/senhaArquivo/inserir?idSenha=${res}`;
            this.senhaService.postFile(file, url, 'arquivo')
              .subscribe((e: HttpProgressEvent | any) => {
                console.log(e);
                if (e.type === 4) {
                  count--;
                  if (count == 0) {
                    this.files = [];
                    this.progress = 0;
                    this.router.navigate(['/senhas/editar', res]).then(() => {
                      this.helper.openSnackBar('Item inserido com sucesso.');
                      this.loadingService.dismiss();
                    })
                  }
                } else {
                  this.progress = Math.round((e.loaded / e.total) * 100);
                  if (isNaN(this.progress)) {
                    this.progress = 100;
                  }
                  this.loadingService.title = `${this.progress}%`;
                }
              })
          });
        } else {
          this.router.navigate(['/senhas']).then(() => {
            this.helper.openSnackBar('Item inserido com sucesso.');
            this.loadingService.dismiss();
          })
        }
      }, e => {
        this.helper.openSnackBar(e.error);
        this.loadingService.dismiss()
      });
  }

  onSelect(event) {
    this.files.push(...event.addedFiles);
  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  filterOptions() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    this.isOptionsEmpty = false;
    const filterValue = value.toLowerCase();
    const options = this.options.filter(option => option.toLowerCase().includes(filterValue));
    if (!options.length) {
      this.isOptionsEmpty = true;
    }
    return options;
  }

  onOptionSelected(event: MatAutocompleteSelectedEvent) {
    if (event.option.value === 'client_add_action') {
      this.submitCliente(this.myControl.value);
    } else {
      const nome = event.option.value;
      const cliente = this.clientes.find(cliente => cliente.nome === nome);
      this.data.cliente = cliente;
    }
  }

  submitCliente(ev) {

    const nome = ev.trim();

    if (this.options.some(value => value === nome)) return;

    const cliente = {
      id: null,
      nome: nome,
      situacao: 'A',
    } as Cliente;

    this.clienteService.post(cliente).subscribe(res => {
      cliente.id = res;
      this.data.cliente = cliente;
      this.options.push(cliente.nome);
      this.helper.openSnackBar('Cliente inserido com sucesso!');
      this.buscarClientes();
    })
  }
}
