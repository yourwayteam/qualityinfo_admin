import { Senha } from 'src/app/models/senha.model';
import { SenhasApi, SenhaService } from 'src/app/services/class/senha.service';
// default
import { LoadingService } from './../../services/loading.service';
import { HelperService } from './../../services/helper.service';
import { GlobalService } from './../../services/global.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ClientesApi, ClienteService } from 'src/app/services/class/cliente.service';
import { Cliente } from 'src/app/models/cliente.model';
import { ModalPasswordComponent } from 'src/app/components/modal-password/modal-password.component';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';

@Component({
  selector: 'app-senhas',
  templateUrl: './senhas.component.html',
  styleUrls: ['./senhas.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class SenhasComponent implements OnInit {

  displayedColumns: string[] = ['1', 'actions'];
  senhasDisplayedColumns: string[] = ['recurso', 'ip', 'porta', 'usuario', 'senha', 'actions'];
  // nome, actions
  expandedElement: Cliente = new Cliente();
  data: Cliente[] = [];
  senhas: Senha[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  search: string = '';
  filterSubscription: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public global: GlobalService,
    public senhaService: SenhaService,
    public clienteService: ClienteService,
    public dialog: MatDialog,
    public helper: HelperService,
    public loadingService: LoadingService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.buscar();
  }

  buscar() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.clienteService.get(this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction.toLocaleUpperCase(), 'A', true, this.search ? this.search : '%');
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.numeroPaginas * this.paginator.pageSize;
          return data.clientes;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe((data: Cliente[]) => {
        this.data = data.map(item => {
          item.loading = false;
          return item;
        });
      });
  }

  filter(e) {
    if (this.paginator.pageIndex > 1) {
      this.paginator.pageIndex = 0;
    }
    if (e) {
      if (this.filterSubscription && !this.filterSubscription.closed) {
        this.filterSubscription.unsubscribe();
      }
      this.filterSubscription = this.clienteService.get(this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction.toLocaleUpperCase(), 'A', true, e.toLocaleLowerCase())
        .subscribe((res: ClientesApi) => {
          this.data = this.paginator.pageIndex == 0 ? res.clientes : this.data.concat(res.clientes);
        });
    } else {
      this.buscar();
    }
  }

  clearFilter() {
    this.search = '';
    this.buscar();
  }

  expandedElementChange(element: Cliente) {
    element.loading = true;
    this.senhaService.getByCliente(-99, -99, element.id)
      .subscribe((res: Senha[]) => {
        setTimeout(() => {
          element.loading = false;
          if (!res.length) return;
          this.senhas = res.map(senha => {
            senha.visibility = false;
            return senha;
          });
          this.expandedElement = this.expandedElement === element ? null : element;
        }, 500);
      }, e => element.loading = false);
  }

  openModalPasswordDialog(senha: Senha) {
    const dialogRef = this.dialog.open(ModalPasswordComponent, {
      width: '1024px',
      data: senha,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    })
  }

  copyToClipboard(str: string) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.helper.openSnackBar('Senha copiada com sucesso!');
  };

  safeValue(str: string): string {
    let password = '';
    for (let index = 0; index < str.length; index++) {
      password += '*';
    }
    return password;
  }
}
