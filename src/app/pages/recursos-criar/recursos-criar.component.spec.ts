import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RecursosCriarComponent } from './recursos-criar.component';

describe('RecursosCriarComponent', () => {
  let component: RecursosCriarComponent;
  let fixture: ComponentFixture<RecursosCriarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RecursosCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecursosCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
