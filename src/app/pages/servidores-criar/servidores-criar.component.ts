import { Servidor } from 'src/app/models/servidor.model';
// Default
import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Router } from '@angular/router';
import { LoadingService } from './../../services/loading.service';
import { HelperService } from './../../services/helper.service';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { ServidorService } from 'src/app/services/class/servidor.service';
@Component({
  selector: 'app-servidores-criar',
  templateUrl: './servidores-criar.component.html',
  styleUrls: ['./servidores-criar.component.scss']
})
export class ServidoresCriarComponent implements OnInit {

  data: Servidor = new Servidor();

  constructor(
    public servidorService: ServidorService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public global: GlobalService
  ) { }

  ngOnInit() {
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Preencha os campos requiridos, por favor!');
      return;
    }

    this.loadingService.present('Inserindo item...');

    this.servidorService.post(this.data)
      .subscribe((res: any) => {
        this.router.navigate(['/servidores']).then(() => {
          this.helper.openSnackBar('Item inserido com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => {
        this.helper.openSnackBar(e.error);
        this.loadingService.dismiss()
      });
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }

}
