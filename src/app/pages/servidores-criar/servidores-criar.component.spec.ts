import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServidoresCriarComponent } from './servidores-criar.component';

describe('ServidoresCriarComponent', () => {
  let component: ServidoresCriarComponent;
  let fixture: ComponentFixture<ServidoresCriarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServidoresCriarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServidoresCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
