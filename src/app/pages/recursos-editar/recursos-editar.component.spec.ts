import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RecursosEditarComponent } from './recursos-editar.component';

describe('RecursosEditarComponent', () => {
  let component: RecursosEditarComponent;
  let fixture: ComponentFixture<RecursosEditarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RecursosEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecursosEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
