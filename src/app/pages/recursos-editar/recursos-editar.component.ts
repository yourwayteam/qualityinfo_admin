import { Recurso } from 'src/app/models/recurso.model';
import { RecursoService } from 'src/app/services/class/recurso.service';
// Default
import { Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from './../../services/loading.service';
import { HelperService } from './../../services/helper.service';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/services/global.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recursos-editar',
  templateUrl: './recursos-editar.component.html',
  styleUrls: ['./recursos-editar.component.scss']
})
export class RecursosEditarComponent implements OnInit {

  data: Recurso = new Recurso();

  buscarSubscription: Subscription;

  constructor(
    public recursoService: RecursoService,
    public helper: HelperService,
    public loadingService: LoadingService,
    public router: Router,
    public route: ActivatedRoute,
    public global: GlobalService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(param => this.buscarUsuario(param.id));
  }

  ngOnDestroy() {
    this.buscarSubscription.unsubscribe();
  }

  buscarUsuario(id: number) {
    this.buscarSubscription = this.recursoService.getById(id)
      .subscribe((res: Recurso) => this.data = res);
  }

  submit(f: NgForm) {

    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      this.helper.openSnackBar('Preencha os campos requiridos, por favor!');
      return;
    }

    this.loadingService.present('Alterando item...');

    this.recursoService.patch(this.data)
      .subscribe((res: any) => {
        this.router.navigate(['/recursos']).then(() => {
          this.helper.openSnackBar('Item alterado com sucesso.');
          this.loadingService.dismiss();
        })
      }, e => {
        this.helper.openSnackBar(e.error);
        this.loadingService.dismiss()
      });
  }

  onSituacaoChange(event: MatSlideToggleChange) {
    event.checked ? this.data.situacao = 'A' : this.data.situacao = 'I';
  }




}
