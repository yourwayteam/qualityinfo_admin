import { LoadingService } from './../../services/loading.service';
import { AuthService } from './../../services/auth.service';
import { HelperService } from './../../services/helper.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalResetPasswordComponent } from 'src/app/components/modal-reset-password/modal-reset-password.component';
import { MatDialog } from '@angular/material/dialog';
import { Usuario } from 'src/app/models/usuario.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: Usuario = new Usuario();

  constructor(
    public helper: HelperService,
    public auth: AuthService,
    public router: Router,
    public loadingService: LoadingService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  login(f: NgForm) {
    if (f.invalid) {
      this.helper.formMarkAllTouched(f);
      return;
    }

    this.loadingService.present('Fazendo o login, aguarde...');

    this.auth.login(this.user).subscribe((res: any) => {
      if (res) {
        this.auth.getUser(this.user).then((r: any) => {
          if (r.solicitaAlteracao == 'S' && (this.user.senha == r.codigoRecuperacao)) {
            this.auth.isRecuperarSenha = true;
            const dialogRef = this.dialog.open(ModalResetPasswordComponent, {
              width: '420px',
            });

            dialogRef.afterClosed().subscribe(result => {
              console.log('The dialog was closed');
            });
          } else {
            this.auth.setToken(btoa(this.user.email + ':' + this.user.senha));
            this.router.navigate(['/']);
          }
          this.loadingService.dismiss()
          this.auth.setUser(r);
        }, e => {
          this.helper.openSnackBar(e.error);
          this.loadingService.dismiss()
        });
      } else {
        this.loadingService.dismiss();
      }
    }, e => {
      this.helper.openSnackBar(e.error);
      this.loadingService.dismiss()
    });
  }

}
