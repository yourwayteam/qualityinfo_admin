import { IsLoggedInGuard } from './guards/is-logged-in.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// pages
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { UsuariosEditarComponent } from './pages/usuarios-editar/usuarios-editar.component';
import { UsuariosCriarComponent } from './pages/usuarios-criar/usuarios-criar.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { ClientesCriarComponent } from './pages/clientes-criar/clientes-criar.component';
import { ClientesEditarComponent } from './pages/clientes-editar/clientes-editar.component';
import { RecursosComponent } from './pages/recursos/recursos.component';
import { RecursosCriarComponent } from './pages/recursos-criar/recursos-criar.component';
import { RecursosEditarComponent } from './pages/recursos-editar/recursos-editar.component';
import { SenhasComponent } from './pages/senhas/senhas.component';
import { SenhasCriarComponent } from './pages/senhas-criar/senhas-criar.component';
import { SenhasEditarComponent } from './pages/senhas-editar/senhas-editar.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { ServidoresComponent } from './pages/servidores/servidores.component';
import { ServidoresCriarComponent } from './pages/servidores-criar/servidores-criar.component';
import { ServidoresEditarComponent } from './pages/servidores-editar/servidores-editar.component';


const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'esqueceu-senha', component: ResetPasswordComponent },
  { path: '', component: HomeComponent, canActivate: [IsLoggedInGuard] },
  { path: 'usuarios', component: UsuariosComponent, canActivate: [IsLoggedInGuard] },
  { path: 'usuarios/adicionar', component: UsuariosCriarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'usuarios/editar/:id', component: UsuariosEditarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'clientes', component: ClientesComponent, canActivate: [IsLoggedInGuard] },
  { path: 'clientes/adicionar', component: ClientesCriarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'clientes/editar/:id', component: ClientesEditarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'recursos', component: RecursosComponent, canActivate: [IsLoggedInGuard] },
  { path: 'recursos/adicionar', component: RecursosCriarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'recursos/editar/:id', component: RecursosEditarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'senhas', component: SenhasComponent, canActivate: [IsLoggedInGuard] },
  { path: 'senhas/adicionar', component: SenhasCriarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'senhas/editar/:id', component: SenhasEditarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'servidores', component: ServidoresComponent, canActivate: [IsLoggedInGuard] },
  { path: 'servidores/adicionar', component: ServidoresCriarComponent, canActivate: [IsLoggedInGuard] },
  { path: 'servidores/editar/:id', component: ServidoresEditarComponent, canActivate: [IsLoggedInGuard] },
  { path: '**', redirectTo: '/not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
