import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Imagem } from 'src/app/models/imagem.model';
import { Senha } from 'src/app/models/senha.model';
import { SenhaService } from 'src/app/services/class/senha.service';
import { GlobalService } from 'src/app/services/global.service';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-modal-password',
  templateUrl: './modal-password.component.html',
  styleUrls: ['./modal-password.component.scss']
})
export class ModalPasswordComponent implements OnInit {

  senha: Senha = new Senha;
  imagens: Imagem[] = [];
  loading: boolean = true;

  showSenhaCliente: boolean = false;
  showSenhaInterno: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<ModalPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Senha,
    public senhaService: SenhaService,
    public helper: HelperService,
    public global: GlobalService
  ) { }

  ngOnInit() {
    this.buscar(this.data.id);
    this.buscarImagens(this.data.id);
  }

  buscar(id: number) {
    this.loading = true;
    this.senhaService.getById(id)
      .subscribe((res: Senha) => {
        this.senha = res;
        this.loading = false;
      }, e => this.loading = false);
  }

  buscarImagens(id: number) {
    this.senhaService.getImagens(id)
      .subscribe((res: Imagem[]) => this.imagens = res);
  }

  close(): void {
    this.dialogRef.close();
  }

  copyToClipboard(str: string) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.helper.openSnackBar('Senha copiada com sucesso!');
  };

  safeValue(str: string): string {
    let password = '';
    for (let index = 0; index < str.length; index++) {
      password += '*';
    }
    return password;
  }

  showImage(url: string) {
    window.open(url, '_blank');
  }

  getFileExtension(fileName: string) {
    return fileName.split('.').pop().toLocaleLowerCase();
  }

  isImage(arquivo: string): boolean {
    let isImage = false;
    const extension = this.getFileExtension(arquivo);
    if (extension === 'png' || extension === 'jpg' || extension === 'jpeg') {
      isImage = true;
    }
    return isImage;
  }

}
