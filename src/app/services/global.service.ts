import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  apiUrl: string = 'http://200.98.201.140:9017';
  imageUrl: string = 'http://200.98.201.140:9017/';
  menuOpen: boolean = false;

  constructor() { }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }

}
