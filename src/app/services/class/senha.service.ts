import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Imagem } from 'src/app/models/imagem.model';
import { Senha } from 'src/app/models/senha.model';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class SenhaService {

  constructor(
    public api: ApiService,
    public auth: AuthService
  ) { }

  get(numeroPagina: number, registroPorPagina: number, ordenacao: string, sentidoOrdenacao: string, filtro: string = '%'): Observable<SenhasApi> {
    return this.api.get(`/senha/buscar?filtro=${filtro}&numeroPagina=${numeroPagina}&registroPorPagina=${registroPorPagina}&ordenacao=${ordenacao}&sentidoOrdenacao=${sentidoOrdenacao}`);
  }

  getById(id): Observable<any> {
    return this.api.get(`/senha/buscar?id=${id}`);
  }

  getByCliente(numeroPagina: number, registroPorPagina: number, idCliente: number): Observable<any> {
    return this.api.get(`/senha/buscar?numeroPagina=${numeroPagina}&registroPorPagina=${registroPorPagina}&idCliente=${idCliente}`);
  }

  getImagens(idSenha: number) {
    return this.api.get(`/senhaArquivo/buscar?idSenha=${idSenha}`);
  }

  deleteImage(idArquivo: number) {
    return this.api.post(`/senhaArquivo/deletar?idArquivo=${idArquivo}`)
  }

  post(senha: Senha): Observable<any> {
    const idUsuario = this.auth.user.id;
    return this.api.post(`/senha/inserir?idUsuario=${idUsuario}`, senha);
  }

  patch(senha: Senha): Observable<any> {
    const idUsuario = this.auth.user.id;
    return this.api.post(`/senha/alterar?idUsuario=${idUsuario}`, senha);
  }

  delete(senha: Senha): Observable<any> {
    return this.api.post('/senha/deletar', senha);
  }

  deleteSelected(senhas: Senha[]): Observable<any> {
    return this.api.post('/senha/deletarLista', senhas);
  }

  postFile(file: File, url: string, fileName: string) {
    return this.api.postFile(file, url, fileName);
  }

  postImages(files: File[], url: string, body: Object) {
    return this.api.postAsFormData(url, body, files);
  }
  
}

export interface SenhasApi {
  senhas: Senha[];
  numeroPaginas: number;
}
