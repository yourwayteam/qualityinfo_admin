import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Servidor } from 'src/app/models/servidor.model';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class ServidorService {

  constructor(
    public api: ApiService
  ) { }

  get(numeroPagina: number, registroPorPagina: number, ordenacao: string, sentidoOrdenacao: string, filtro: string = '%'): Observable<ServidoresApi> {
    return this.api.get(`/servidor/buscar?filtro=${filtro}&numeroPagina=${numeroPagina}&registroPorPagina=${registroPorPagina}&ordenacao=${ordenacao}&sentidoOrdenacao=${sentidoOrdenacao}`);
  }

  getById(id): Observable<any> {
    return this.api.get(`/servidor/buscar?id=${id}`);
  }

  getSelect() {
    return this.api.get(`/select/servidor/buscar`);
  }

  post(servidor: Servidor): Observable<any> {
    return this.api.post('/servidor/inserir', servidor);
  }

  patch(servidor: Servidor): Observable<any> {
    return this.api.post('/servidor/alterar', servidor);
  }

  delete(servidor: Servidor): Observable<any> {
    return this.api.post('/servidor/deletar', servidor);
  }

  deleteSelected(servidores: Servidor[]): Observable<any> {
    return this.api.post('/servidor/deletarLista', servidores);
  }

  postFile(file: File, url: string, fileName: string) {
    return this.api.postFile(file, url, fileName);
  }

  compareFn(v1: Servidor, v2: Servidor): boolean {
    return v1 && v2 ? v1.id === v2.id : v1 === v2
  }
}

export interface ServidoresApi {
  servidores: Servidor[];
  numeroPaginas: number;
}
