import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Recurso } from 'src/app/models/recurso.model';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class RecursoService {

  constructor(
    public api: ApiService
  ) { }

  get(numeroPagina: number, registroPorPagina: number, ordenacao: string, sentidoOrdenacao: string, filtro: string = '%'): Observable<RecursosApi> {
    return this.api.get(`/recurso/buscar?filtro=${filtro}&numeroPagina=${numeroPagina}&registroPorPagina=${registroPorPagina}&ordenacao=${ordenacao}&sentidoOrdenacao=${sentidoOrdenacao}`);
  }

  getById(id): Observable<any> {
    return this.api.get(`/recurso/buscar?id=${id}`);
  }

  post(recurso: Recurso): Observable<any> {
    return this.api.post('/recurso/inserir', recurso);
  }

  patch(recurso: Recurso): Observable<any> {
    return this.api.post('/recurso/alterar', recurso);
  }

  delete(recurso: Recurso): Observable<any> {
    return this.api.post('/recurso/deletar', recurso);
  }

  deleteSelected(recursos: Recurso[]): Observable<any> {
    return this.api.post('/recurso/deletarLista', recursos);
  }

  postFile(file: File, url: string, fileName: string) {
    return this.api.postFile(file, url, fileName);
  }

  compareFn(v1: Recurso, v2: Recurso): boolean {
    return v1 && v2 ? v1.id === v2.id : v1 === v2
  }
}

export interface RecursosApi {
  recursos: Recurso[];
  numeroPaginas: number;
}
