export class Usuario {

    id: number;
    email: string = '';
    senha: string = '';
    nome: string = '';
    sobrenome: string = '';
    situacao: string = 'A';
    [x: string]: any;
    constructor(obj?) {
        Object.assign(this);
    }
}