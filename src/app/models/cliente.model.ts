export class Cliente {

    id: number;
    nome: string = '';
    situacao: string = 'A';
    [x: string]: any;
    constructor(obj?) {
        Object.assign(this);
    }
}