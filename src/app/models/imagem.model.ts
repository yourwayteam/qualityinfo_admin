export interface Imagem {
    id: number;
    imagem: string;
    nome: string;
}