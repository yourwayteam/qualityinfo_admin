import { Cliente } from './cliente.model'
import { Recurso } from './recurso.model'
import { Servidor } from './servidor.model';

export class Senha {

    id: number;
    cliente: Cliente = new Cliente();
    recurso: Recurso;
    enderecoIp: string = '';
    porta: number;
    usuarioInterno: string = '';
    senhaInterno: string = '';
    usuarioCliente: string = '';
    senhaCliente: string = '';
    observacao: string = '';
    idUsuarioCriacao: number;
    idUsuarioAlteracao: number;
    dataHoraCriacao: Date;
    dataHoraAlteracao: Date;
    servidor: Servidor = new Servidor();
    [x: string]: any;
    constructor(obj?) {
        Object.assign(this);
    }
}